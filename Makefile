# 
# Small makefile for Solaire
#

CC=g++

all: projet

projet: projet.o  
	$(CC) -o projet projet.o -lGL -lglut  -lm -lGLEW -lGLU -larmadillo -llapack -lblas

projet.o: projet.cpp  
	$(CC) -c projet.cpp -lGL -lglut  -lm -lGLEW -lGLU -larmadillo -llapack -lblas

clean :
	rm projet.o
