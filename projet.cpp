#ifdef __APPLE__
#include <GLUT/glut.h> /* Pour Mac OS X */
#else
#include <GL/glut.h>   /* Pour les autres systemes */
#endif
#include <cstdlib>
#include <GL/freeglut.h>
#include <math.h>  
#include <armadillo>

using namespace arma;

//variables saisieinteractive
const int NMAX = 100;
int N = 5; // NBR DE POINTS
int mp=-1,droite=0,gauche=0;
float s = 0.5;
int n = 10;

struct Point {
	float x,y,z;
	Point(float a=0, float b=0, float c=0) {set(a,b,c);}
	void set(float a, float b, float c) {x=a;y=b;z=c;}
};





/*
Point P_cubic1[NMAX] = {{0,0,3}, {0,4,2}, {3,0.25,4}, {4,4,0}};
Point P_cubic2[NMAX] = {{0.5,0.5,0.5}, {1,7,8}, {1.5,1.5,1.5}, {4,4,4}};
Point P_cubic3[NMAX] = {{0.75,0.75,0.75}, {1,3,1}, {3,3,3}, {4,4,4}};
Point P_cubic4[NMAX] = {{0.25,0.25,0.25}, {1,1,1}, {3,3,3}, {4,4,4}};
*/


char presse;
int anglex,angley,x,y,xold,yold;

/* Prototype des fonctions */
void affichage();
void clavier(unsigned char touche,int x,int y);
void reshape(int x,int y);
void idle();
void mouse(int bouton,int etat,int x,int y);
void mousemotion(int x,int y);
void CatmullRom();


int main(int argc,char **argv)
{
  /* initialisation de glut et creation
     de la fenetre */
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowPosition(200,200);
  glutInitWindowSize(1000,1000);
  glutCreateWindow("cube");

  /* Initialisation d'OpenGL */
  glClearColor(0.0,0.0,0.0,0.0);
  glColor3f(1.0,1.0,1.0);
  glPointSize(2.0);
  glEnable(GL_DEPTH_TEST);


  /* enregistrement des fonctions de rappel */
  glutDisplayFunc(affichage);
  glutKeyboardFunc(clavier);
  glutReshapeFunc(reshape);
  glutMouseFunc(mouse);
  glutMotionFunc(mousemotion);

  /* Entree dans la boucle principale glut */
  glutMainLoop();
  return 0;
}
float angle;



void affichage()
{
  int i,j;

  /* effacement de l'image avec la couleur de fond */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
glShadeModel(GL_SMOOTH);

  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
    glOrtho(-7,7,-7,7,-7,7);

  glRotatef(angley,1.0,0.0,0.0);
  glRotatef(anglex,0.0,1.0,0.0);

   // Dessin du socle
    glRotatef(angle,1,1,0); //anim


    


    
  CatmullRom();

    

    




    //Repère
    //axe x en rouge
    glBegin(GL_LINES);
        glColor3f(1.0,0.0,0.0);
    	glVertex3f(0, 0,0.0);
    	glVertex3f(1, 0,0.0);
    glEnd();
    //axe des y en vert
    glBegin(GL_LINES);
    	glColor3f(0.0,1.0,0.0);
    	glVertex3f(0, 0,0.0);
    	glVertex3f(0, 1,0.0);
    glEnd();
    //axe des z en bleu
    glBegin(GL_LINES);
    	glColor3f(0.0,0.0,1.0);
    	glVertex3f(0, 0,0.0);
    	glVertex3f(0, 0,1.0);
    glEnd();

  glFlush();

  //On echange les buffers
  glutSwapBuffers();
}

void clavier(unsigned char touche,int x,int y)
{
  switch (touche)
    {
    case 'p': /* affichage du carre plein */
      glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
      glutPostRedisplay();
      break;
    case 'f': /* affichage en mode fil de fer */
      glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
      glutPostRedisplay();
      break;
    case 's' : /* Affichage en mode sommets seuls */
      glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
      glutPostRedisplay();
      break;
    case 'd':
      glEnable(GL_DEPTH_TEST);
      glutPostRedisplay();
      break;
    case 'D':
      glDisable(GL_DEPTH_TEST);
      glutPostRedisplay();
      break;
    case 'q' : /*la touche 'q' permet de quitter le programme */
      exit(0);
    }
}

void reshape(int x,int y)
{
  if (x<y)
    glViewport(0,(y-x)/2,x,x);
  else
    glViewport((x-y)/2,0,y,y);
}

void mouse(int button, int state,int x,int y)
{
  /* si on appuie sur le bouton gauche */
  if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
  {
    presse = 1; /* le booleen presse passe a 1 (vrai) */
    xold = x; /* on sauvegarde la position de la souris */
    yold=y;
  }
  /* si on relache le bouton gauche */
  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    presse=0; /* le booleen presse passe a 0 (faux) */
}

void mousemotion(int x,int y)
  {
    if (presse) /* si le bouton gauche est presse */
    {
      /* on modifie les angles de rotation de l'objet
	 en fonction de la position actuelle de la souris et de la derniere
	 position sauvegardee */
      anglex=anglex+(x-xold);
      angley=angley+(y-yold);
      glutPostRedisplay(); /* on demande un rafraichissement de l'affichage */
    }

    xold=x; /* sauvegarde des valeurs courante de le position de la souris */
    yold=y;
  }

void CatmullRom()
{

	float tc;
  float tu;

	Mat<float> cx, cy, cz;
	Mat<float> Mcat = {{-s,2-s,s-2,s},{2*s,s-3,3-2*s,-s},{-s,0,s,0},{0,1,0,0}};
	Col<float> Px, Py, Pz;
	Row<float> Ut;
  Col<float> Tt;
	float couleur = 0;

  Mat<float> Points_x(5,5);
  Mat<float> Points_y(5,5);
  Mat<float> Points_z(5,5);


  Mat<float> P_x = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
  Mat<float> P_y = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
  Mat<float> P_z = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};



// generation des points pour la matrice de points de controle
for(int i=0; i<N;i++)
{
  for(int j=0; j<N;j++)
  {
    Points_x(i,j)= i;
    Points_y(i,j)= j;
    Points_z(i,j)= sin(i) * cos(j);;
  }
}

  
	


  glPushMatrix();
	glPointSize(3.0);
	glColor3f(0.0,1.0,0.0);
	glBegin(GL_POINTS);
	for (int i=0;i<N;i++){
		//glVertex3f(Points_x,P_cubic1[i].y,P_cubic1[i].z);
    
	}
	glEnd();





	glColor3f(1.0,0.0,0.0);
	float theta = 0;

  
    for (int i = 0; i < N-3; i++)
    {
      for(int j=0; j< N-3;j++)
      {
        

    
        glBegin(GL_LINE_STRIP);
        for(int t=0; t<100;t++){
          for(int u=0; u<100; u++)
            {

                P_x = { {Points_x(i+3,j), Points_x(i+3,j+1), Points_x(i+3,j+2), Points_x(i+3,j+3)} ,
                      {Points_x(i+2,j), Points_x(i+2,j+1), Points_x(i+2,j+2), Points_x(i+2,j+3)} ,
                      {Points_x(i+1,j), Points_x(i+1,j+1), Points_x(i+1,j+2), Points_x(i+1,j+3)} ,
                      {Points_x(i,j), Points_x(i,j+1), Points_x(i,j+2), Points_x(i,j+3)}
                };

                P_y = { {Points_y(i+3,j), Points_y(i+3,j+1), Points_y(i+3,j+2), Points_y(i+3,j+3)} ,
                      {Points_y(i+2,j), Points_y(i+2,j+1), Points_y(i+2,j+2), Points_y(i+2,j+3)} ,
                      {Points_y(i+1,j), Points_y(i+1,j+1), Points_y(i+1,j+2), Points_y(i+1,j+3)} ,
                      {Points_y(i,j), Points_y(i,j+1), Points_y(i,j+2), Points_y(i,j+3)}
                };

                P_z = { {Points_z(i+3,j), Points_z(i+3,j+1), Points_z(i+3,j+2), Points_z(i+3,j+3)} ,
                      {Points_z(i+2,j), Points_z(i+2,j+1), Points_z(i+2,j+2), Points_z(i+2,j+3)} ,
                      {Points_z(i+1,j), Points_z(i+1,j+1), Points_z(i+1,j+2), Points_z(i+1,j+3)} ,
                      {Points_z(i,j), Points_z(i,j+1), Points_z(i,j+2), Points_z(i,j+3)}
                };



            tc = (float) t/100;
            tu = (float) u/100;
            Tt = {tc*tc*tc,tc*tc,tc,1};
            Ut = {tu*tu*tu,tu*tu,tu,1};

            cx= Ut * Mcat * P_x * trans(Mcat) * Tt;
            cy= Ut * Mcat * P_y * trans(Mcat) * Tt;
            cz= Ut * Mcat * P_z * trans(Mcat) * Tt;				

            glColor3f(0.75,couleur,0.0);
            glVertex3f(cx(0,0),cy(0,0),cz(0,0));
            }
          
			}

			glEnd();

    }
    
  }

  
		
    

		couleur += 1.0/n;
	

     glPopMatrix();
    
}

